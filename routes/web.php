<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('customers.order.index');
})->middleware(['auth'])->name('dashboard');

Route::resource('home', HomeController::class)->only([
    'index'
]);
Route::get('show/{id}', [HomeController::class, 'show'])->name('home.show');

Route::resource('order', OrderController::class)->only([
    'index','create'
]);

Route::get('/sort', [OrderController::class, 'sort'])->name('order.sort');
Route::get('/sortorderbycustomer', [OrderController::class, 'orderByCustomner'])->name('order.orderByCustomner');
Route::get('/orderIphone', [OrderController::class, 'orderIphone'])->name('order.orderIphone');

require __DIR__ . '/auth.php';
