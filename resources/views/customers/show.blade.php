@extends('layouts.master')
@section('content')
    <div style="margin-left: 10% ">
        <div class="card">
            <div class="imgBx">
                <img src="../dist/img/1.png">
            </div>
            <div class="contentBx">
                <h2>{{ $products->name }}</h2>
                <div class="size">
                    <h3>Size : pro,pro max</h3>
                </div>
                <div class="color">
                    <h3>Color :</h3>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="size">
                    <h3>Price: {{ $products->price }}$</h3>
                </div>
            </div>
        </div>
    </div>
@endsection

