@extends('layouts.master')
@section('content')
<a style="width: 10%;height:10%;margin-left: 7%;padding: 10px;text-decoration: none;color:aliceblue;background-color: rgb(166, 214, 166)" href="{{ route('order.create') }}">Add</a>

    <div style="margin-left: 10%" class="container">
        <div class="row">

            <div class="col-md-9">
                <label style="text-align: center" for="">
                    <h2>Order</h2>
                </label>

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td> {{ $order->id }}</td>
                                <td>{{ $order->name }}</td>
                                <td>{{ $order->date }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $orders->links() }}
            </div>

            <div class="col-md-3">
                <label for="">
                    <h2>Order Detail</h2>
                </label>

                <table class="table">
                    <thead class="thead-dark">
                        <tr style="text-align: center">
                            <th>#</th>
                            <th>Name Product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr style="margin: 10px">
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->price }}$</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection
