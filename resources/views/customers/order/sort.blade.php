 @extends('layouts.master')
 @section('content')
     <div style="margin-left: 10%" >
         <label style="text-align: center" for="">
             <h2>Order Detail today</h2>
         </label>

         <table class="table">
            <thead class="thead-dark">
                 <tr style="text-align: center">
                     <th>#</th>
                     <th>Name</th>
                     <th>Address</th>
                     <th>Phone</th>
                     <th>Email</th>
                     <th>Date</th>
                 </tr>
             </thead>
             <tbody>
                 @foreach ($customers as $customer)
                     @if ($customer)
                         <tr style="margin: 10px">
                             <td> {{ $customer->id }}</td>
                             <td>{{ $customer->name }}</td>
                             <td>{{ $customer->address }}</td>
                             <td>{{ $customer->phone }}</td>
                             <td>{{ $customer->email }}</td>
                             <td>{{ $customer->date }}</td>
                         </tr>
                     @else
                         There is no record
                     @endif
                 @endforeach

             </tbody>
         </table>
         {{ $customers->links() }}
     </div>
 @endsection
