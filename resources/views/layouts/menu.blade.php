<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="{{ route('home.index') }}" class="brand-link">
        <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <div class="sidebar">

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>


        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fa-solid fa-list"></i>
                        <p>
                            List
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('order.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List order and details</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('order.sort') }}" class="nav-link">

                                <i class="far fa-circle nav-icon"></i>
                                <p>All order in this month </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('order.orderByCustomner') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All orders by customer</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a
                            href="{{ route('order.orderIphone') }}"
                            class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All orders by customer that bought an Iphone</p>
                            </a>
                        </li>
                    </ul>
                </li>
</aside>
