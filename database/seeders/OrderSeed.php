<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Order;
use Illuminate\Database\Seeder;

class OrderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::factory()
        ->has(
            Order::factory()->count(5)
            ) // method count: muốn tạo ra bao nhiêu record.
        ->count(10)
        ->create(); // method create() : thực thi lệnh
    }
}
