<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Database\Seeder;

class OrderDetailSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::factory()
        ->has(
            OrderDetail::factory()->count(5)
            ) // method count: muốn tạo ra bao nhiêu record.
        ->count(10)
        ->create(); // method create() : thực thi lệnh
        Product::factory()
        ->has(
            OrderDetail::factory()->count(5)
            ) // method count: muốn tạo ra bao nhiêu record.
        ->count(10)
        ->create(); // method create() : thực thi lệnh
    }
}
