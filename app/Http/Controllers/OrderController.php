<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=[];
        $orders = Order::join('customers', 'customers.id', '=', 'orders.customer_id')->paginate(10);
        // $customers = Customer::join('orders', 'orders.customer_id', '=', 'customers.id')
        // ->paginate(10);
        $products = Product::join('order_details', 'order_details.product_id', '=', 'products.id')->paginate(10);

        $data['orders'] = $orders;
        $data['products'] = $products;

        return view('customers.order.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sort(){
        $data = [];
        $customers = Customer::join('orders', 'orders.customer_id', '=', 'customers.id')
        ->whereMonth('orders.date',Carbon::now()->month)
        ->whereYear('orders.date',Carbon::now()->year)
        ->paginate(10);
        $data['customers'] = $customers;
        return view('customers.order.sort',$data);
    }
    public function orderByCustomner(){
        $data = [];
        $customers = Customer::join('orders', 'orders.customer_id', '=', 'customers.id')
        ->paginate(10);
        $data['customers']=$customers;
        return view('customers.order.orderByCustomer',$data);
    }
    public function orderIphone(){
        $data = [];
        // $customers = Customer::join('orders', 'orders.customer_id', '=', 'customers.id')
        //     ->join('order_details','orders.id','=','order_details.order_id')
        //     ->join('products','order_details.product_id','=','products.id')
        // ->paginate(10);
        $orderIphone = Order::where('product_id')->get();
        //  dd($orderIphone);
        $data['customers']=$customers;
        return view('customers.order.sortOrderIphone',$data);
    }
}
